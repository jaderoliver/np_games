## Assessment for Napoleon Games
 
 This is a sample use case to spin up a cluter k8s and pod using the stacks.

 * Pulumi
 * Typesscript
 * Google Cloud
 * Kubernetes

### Prerequisites

  1. [Install Pulumi] (https://www.pulumi.com/docs/get-started/install/)
  1. [Install Node.js] (https://nodejs.org/en/download/)
  1. [Install Google Cloud SDK] (https://cloud.google.com/sdk/docs/downloads-interactive)
  1. [Config GCP Account] (https://www.pulumi.com/docs/get-started/gcp/begin/)

### Steps

  1. Clone the repo
        ```bash
            git clone https://jaderoliver@bitbucket.org/jaderoliver/np_games.git
        ```
  1. Install depencies Packages
        ```bash
            $npm install
        ```
  1. Define required GCP variables
        ```bash
            pulumi config set gcp:project <NAME_OR_ID_FROM_YOUR_PROJECT>
            pulumi config set gcp:zone 
        ```
