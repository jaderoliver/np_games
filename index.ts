import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import * as fs from "fs";
import { k8sConfig, k8sProvider } from "./cluster";

const name = `${pulumi.getProject()}-${pulumi.getStack()}`;
const appLabels = { app: `npgame-${name}` };

const indexConf = new k8s.core.v1.ConfigMap(name, {
        metadata: { labels: appLabels },
        data: { "index.html": fs.readFileSync("index.html").toString() },
    },
    {
        provider: k8sProvider,
    },
);
const confMap = indexConf.metadata.apply(m => m.name);

const deployment = new k8s.apps.v1.Deployment(name,
    {
        spec: {
            replicas: 1,
            selector: { matchLabels: appLabels },
            template: {
                metadata: {
                    labels: appLabels
                },
                spec: {
                    containers: [
                        {
                            name: "nginx",
                            image: "nginx:latest",
                            ports: [{ name: "http", containerPort: 80 }],
                            volumeMounts: [{ name: "ng-index-file", mountPath: "/usr/share/nginx/html" }],
                        },
                    ],
                    volumes: [{ name: "ng-index-file", configMap: { name: confMap } }],
                },
            },
        },
    },
    {
        provider: k8sProvider,
    },
);

export const deploymentName = deployment.metadata.name;

const service = new k8s.core.v1.Service(name,
    {
        metadata: {
            labels: appLabels
        },
        spec: {
            type: "LoadBalancer",
            ports: [{ port: 80, targetPort: "http" }],
            selector: appLabels,
        },
    },
    {
        provider: k8sProvider,
    },
);

export const serviceName = service.metadata.name;
export const servicePublicIP = service.status.loadBalancer.ingress[0].ip;