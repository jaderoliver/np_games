import * as gcp from "@pulumi/gcp";
import { Config } from "@pulumi/pulumi";
import * as random from "@pulumi/random";

const config = new Config();

export const nameCluster = config.get("nameCluster") || "ngcluster";
export const nodeCount = config.getNumber("nodeCount") || 2;
export const nodeMachineType = config.get("nodeMachineType") || "n1-standard-1";
export const username = config.get("username") || "ngadmin";
export const password = config.get("password") || new random.RandomPassword(
    "password", { length: 15, special: true }).result;
export const masterVersion = config.get("masterVersion") ||
    gcp.container.getEngineVersions().then(it => it.latestMasterVersion);